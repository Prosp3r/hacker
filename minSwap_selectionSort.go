package main

import (
	"fmt"
	"reflect"
	"sort"
)

func swapEm(Q []int32) int32 {
	I := int32(0)
	ER := make([]int32, len(Q))
	for i := range Q {
		ER[i] = Q[i]
	}
	sort.Slice(ER, func(i, j int) bool { return ER[int32(i)] < ER[int32(j)] })

	fmt.Println(Q)
	fmt.Println(ER) //Expected Results
	if reflect.DeepEqual(ER, Q) == true {
		return I
	}
	L := int32(len(Q))
	for x := int32(0); x < L; x++ {
		//fmt.Println(x)
		minInd := x
		for j := x; j < L; j++ {
			if Q[j] < Q[minInd] {
				minInd = j
			}
		}
		Q[x], Q[minInd] = Q[minInd], Q[x]
		I++
	}
	fmt.Println(Q)

	return I
}

//
func minSwap(Q []int32) {
	//I := int32(0)
	ms := swapEm(Q)
	fmt.Println(ms)
}

func main() {

	//Q := []int32{1, 3, 5, 2, 4, 6, 7}
	Q := []int32{7, 1, 3, 2, 4, 5, 6}
	minSwap(Q)
}
