package main

import (
	"fmt"
	"sort"
)

// Complete the arrayManipulation function below.
func arrayManipulation(M int32, Q [][]int32) int64 {
	//maxOps := 0
	//
	I := 1
	QZ := len(Q)     //size of array
	Q1z := len(Q[I]) //size of inner array
	fmt.Printf("Size of inner array ie. : array width : %v ", Q1z)
	fmt.Printf("Size of outer array width : %v ", QZ)
	//fmt.Println(Q1z)
	K := make([]int32, 0)
	OpsArray := make([]int32, 0)
	firstArray := make([]int32, 0)

	for _, v := range Q {
		K = append(K, v[len(Q)-1])                          //last array column
		OpsArray = append(OpsArray, v[len(Q)-2])            //second to last column
		firstArray = append(firstArray, v[(len(Q))-len(Q)]) //first column
	}

	var OR []int32 //sort ops array
	for _, v := range OpsArray {
		OR = append(OR, v)
	}
	sort.Slice(OR, func(i, j int) bool { return OR[int32(i)] < OR[int32(j)] })
	maxOps := OR[len(OR)-1] //5 already minus one for an array of five

	//ARRAY ManIPulation
	//OPS1 :
	OPS := make([][]int32, M)
	//CREATE (amount: M = number of operations) STORAGE ARRAYSLICES FOR OPERATIONS SIZE OF MAXOPS
	check := int32(0)
	for x := int32(0); x < M; x++ {

		fmt.Printf("\nOperations : %v \n", OPS[x+0])
		fmt.Printf("Check : %v \n", check)
		//fmt.Printf("K value : %v \n", K[x])
		fmt.Printf("M value : %v \n", M)
		fmt.Printf("Second ops column : %v \n", OpsArray)
		fmt.Printf("First ops column : %v \n", firstArray)
		fmt.Printf("Maximum ops : %v \n", maxOps)
		OPS[x] = make([]int32, maxOps) //giving us three zero values indexes
		//OPS[x] = make([]int32,{}) //giving us three zero values indexes
		//POPULATE THE INNER ARRAYS
		//fmt.Printf("OPSArray is: %v \n", OpsArray)
		//fmt.Printf("OPSArray[x] is: %v \n", OpsArray[x])

		//for o := int32(firstArray[x] - 1); o < OpsArray[x]; o++ {
		//for o := int32(firstArray[x] - 1); o < K[x]; o++ {
		for o := int32(firstArray[x] - 1); o < OpsArray[x]; o++ {
			OPS[x][o] = K[x] + OPS[x][o]
			fmt.Println(OPS)
			//OPS[x][o] = K[x] + OPS[x][o]
		}

		//OPS[x] = append(OPS[x], opsval) //append the real ops value

		fmt.Printf("OPS[%v] value : %v \n", x, OPS[x][check])
		//cyclical within the array witch limits
		if check > maxOps {
			check = 0
		}
		check++

	}

	var rzmap [][]int32
	for _, v := range OPS {
		rzmap = append(rzmap, v)
	}

	fmt.Println(rzmap, len(rzmap[0]))

	SUMUPS := make([]int32, int32(len(rzmap[0])))
	//var SUMUPS []int32

	//::::::::::::::::::::::::::
	//::::::::::::::::::::::::::
	/*j := 0
	for x := int32(0); x < int32(len(rzmap)); x++ {
		d := int32(0)
		fmt.Println(rzmap[x])
		for o := int32(0); o < int32(len(SUMUPS)); o++ {
			fmt.Println(rzmap[x][o])
			d++
		}
		fmt.Printf("d is: %v j is %v \n", j, d)
		j++
	}*/
	//:::::::::::::::::::::::::::
	//:::::::::::::::::::::::::::
	j := int32(0)
	for o := int32(0); o < int32(len(SUMUPS)); o++ {
		d := int32(0)
		for x := int32(0); x < int32(len(rzmap)); x++ {

			fmt.Println(rzmap[x])
			fmt.Println(rzmap[x][o])

			//if d > 0 {
			//fmt.Printf("SUMUP[j-j] is now : %v \n", SUMUPS[d-1])
			SUMUPS[j] = (SUMUPS[j] + rzmap[d][j])
			//SUMUPS[j] = (rzmap[d-1][j] + rzmap[d][j])
			//} else {
			//	SUMUPS[j] = SUMUPS[j] + rzmap[d][j]
			//}
			//100 100 200 200 100   SUMUPS
			//[100 100 0 0 0] [0 100 100 100 100] [0 0 100 100 0]

			fmt.Printf(":::::d is %v \n", d)
			fmt.Printf(":::::j is %v \n", j)
			//fmt.Printf(":::::j-1 is %v \n", j-1)
			//fmt.Printf("SUMUP[j-j] is now : %v \n", SUMUPS[j-j])
			fmt.Printf("SUMUP is now : %v \n", SUMUPS)
			fmt.Println(OPS)

			d++
		}
		fmt.Printf("j is: %v d is %v \n", j, d)
		j++
	}

	//:::::::::::::::::::::::::::
	//:::::::::::::::::::::::::::

	//Lrzmap := len(rzmap)
	//fmt.Printf(":::::M+int32(len(OPS[0])-1) %v \n", maxOps)
	//SUMOPS := make([]int32, M+int32(len(OPS[0])-1))
	//SUMOPS := make([]int32, maxOps)
	/*for x := int32(0); x < M; x++ {

		//for o := int32(firstArray[x] - 1); o < maxOps; o++ {
		J := 0
		//for o := int32(0); o < maxOps-2; o++ {
		for o := int32(0); o < int32(len(OPS[0])-2); o++ {
			fmt.Printf("O is now : %v \n", o)
			//fmt.Printf("OPS[%v][%v] : %v \n", x, o, OPS[J][x])
			//fmt.Printf("OPS[%v][%v] : %v \n", x, o, OPS[x][o])
			//SUMOPS[x] += OPS[x][o]
			//fmt.Printf("OPS[x] is: %v long \n", x, len(OPS[x]))
			SUMOPS[x] += OPS[o][x]
			J++
		}
	}*/

	//fmt.Println(OPS[1][4])
	fmt.Println(SUMUPS)
	sort.Slice(SUMUPS, func(i, j int) bool { return SUMUPS[int32(i)] < SUMUPS[int32(j)] })
	fmt.Printf("Fucking thing is : %v \n", SUMUPS[len(SUMUPS)-1])
	fmt.Println(OPS)
	//fmt.Printf("OPS[x] is: %v long \n", len(OPS[0]))

	//for check := 0; check <= len(Q)-2; check++ {
	//Number of times to run operations
	//times := (v[len(Q)-2] - v[(len(Q))-len(Q)]) + 1

	//}

	//fmt.Println(firstArray)

	//QmaxSize :=

	//fmt.Println(maxOps)
	//for i := range Q {
	//	fmt.Println(len(Q[i]))
	//}
	return 0
}

func main() {
	//M := int32(3) //number of ops
	//Q := [][]int32{{1, 2, 100}, {2, 5, 100}, {3, 4, 100}}
	M := int32(10) //number of ops
	Q := [][]int32{{1, 5, 3}, {4, 8, 7}, {6, 9, 1}}
	arrayManipulation(M, Q)
}
