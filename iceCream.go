package main

import (
	"fmt"
	"sync"
	"time"
)

func measureTime(funcName string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("Time taken by %s function is %v \n", funcName, time.Since(start))
	}
}

//
func factCost(cost []int32, money, rmoney int32, i int32) int {
	//fmt.Println(cost)

	//FULL MONEY SUBTRACTION LOOP
	//for y := i + 1; y < int32(len(cost)); y++ {
	//	if cost[i]+cost[y] == money {
	//		fmt.Printf("%v %v \n", i+1, y+1)
	//		return 0
	//	}
	//}
	//END OF FULL MONEY LOOP

	//REMAINING MONEY LOOP
	//rmoney := money - cost[i]
	for in, v := range cost {

		if v == rmoney {
			//fmt.Printf("Cost[%v] : for RMoney => %v \n", in, rmoney)
			if int32(in) == i {

			} else {
				//swap low and high
				if i < int32(in) {
					fmt.Printf("%v %v \n", i+1, in+1)
					return 0
				}
				fmt.Printf("%v %v \n", in+1, i+1)
				return 0

			}
		}
	}
	//END OF REMAINING MONEY LOOP

	if i >= int32(len(cost)-1) {
		return 0
	}
	i++
	rmoney = money - cost[i]
	return len(cost) * factCost(cost, money, rmoney, i)
}

//WITH MAPS
func whatFlavorsx(cost []int32, money int32) {
	defer measureTime("whatFlavorsxxx")()
	rmoney := money - cost[0]
	factCost(cost, money, rmoney, 0)

}

//
func factCostConcurrent(cost []int32, money, rmoney int32, i int32, endIndex, routinecount int, c chan int32) int32 {

	//REMAINING MONEY LOOP
	//rmoney := money - cost[i]
	//startIndex := i * int32(routinecount)
	for in, v := range cost {
		if v == rmoney {
			if int32(in) == i {
				//do nothing continue loop
			} else {
				//swap low and high
				//if i < int32(in) {
				//fmt.Printf("%v %v \n", i+1, in+1)
				//	c <- in
				//	break
				//return 0
				//}
				//fmt.Printf("%v %v \n", in+1, i+1)
				if int32(in) > i {
					c <- i + 1         //, i+1
					c <- int32(in) + 1 //, i+1
					break
				} else {
					c <- int32(in) + 1 //, i+1
					c <- i + 1         //, i+1
					break
				}
			}
		}
	}
	//END OF REMAINING MONEY LOOP

	//if i >= int32(len(cost)-1) {
	if i >= (int32(endIndex*routinecount) - 1) {
		return 0
	}
	i++
	rmoney = money - cost[i]
	return int32(len(cost)) * factCostConcurrent(cost, money, rmoney, i, endIndex, routinecount, c)
}

var wg sync.WaitGroup // 1

//WITH MAPS
func whatFlavorsConcurrent(cost []int32, money int32) {
	//cpucores := runtime.NumCPU()
	//fmt.Printf("I have %v CPU Cores \n", cpucores)

	//for i := 0; i < 10; i++ {
	//	wg.Add(1)     // 2
	//	go routine(i) // *
	//}
	//wg.Wait() // 4

	defer measureTime("whatFlavorsxxx")()
	rmoney := money - cost[0]

	c := make(chan int32)

	worksize := len(cost)
	indi := 0
	if worksize > 10000 {
		indi = worksize / 4
		endIndex := indi
		wg.Add(1) // 2
		go func() { c <- factCostConcurrent(cost, money, rmoney, int32(indi), endIndex, 0, c) }()
		go func() { c <- factCostConcurrent(cost, money, rmoney, int32(indi), endIndex, 1, c) }()
		go func() { c <- factCostConcurrent(cost, money, rmoney, int32(indi), endIndex, 2, c) }()
		go func() { c <- factCostConcurrent(cost, money, rmoney, int32(indi), endIndex, 3, c) }()
	}
	//not so heavy
	endIndex := worksize / 1
	go func() { c <- factCostConcurrent(cost, money, rmoney, int32(indi), endIndex, 0, c) }()

	result := make([]int32, 0)

	for i := 0; i <= indi+1; i++ {
		result = append(result, <-c)
	}
	fmt.Printf("%v %v \n", result[0], result[1])
	wg.Wait() //
}

func routine(i int) {
	defer wg.Done() // 3
	fmt.Printf("routine %v finished\n", i)
}

func main() {
	//cpucores := runtime.NumCPU()
	//fmt.Printf("I have %v CPU Cores \n", cpucores)

	cb := []int32{2, 2, 4, 3}
	//2 2 4 3
	//cb := []int32{1, 4, 5, 3, 2}
	cosb := int32(4)
	//cb := []int32{7, 2, 5, 4, 11}
	//cosb := int32(12)
	//cosb := int32(6)

	fmt.Println(cb)
	fmt.Printf("Looking for cost : %v \n", cosb)

	whatFlavorsx(cb, cosb)

	//ca := []int32{1, 4, 5, 3, 2}
	ca := []int32{4, 3, 2, 5, 7}
	cosa := int32(8)
	fmt.Println(ca)
	fmt.Printf("Looking for cost : %v \n", cosa)

	whatFlavorsx(ca, cosa)

	cc := []int32{1, 2, 3, 5, 6}
	cosc := int32(5)
	fmt.Println(cc)
	fmt.Printf("Looking for cost : %v \n", cosc)

	//whatFlavorsConcurrent(cc, cosc)
	whatFlavorsx(cc, cosc)
	//factCost(ca, cosa, 0)

	//cd := []int32{2, 2, 4, 3}
	//2 2 4 3
	//cd := []int32{1, 4, 5, 3, 2}
	cd := []int32{7, 2, 5, 4, 11, 20, 21, 1, 9, 12, 10, 15, 18, 31, 35, 60, 41, 65, 22, 33, 90, 45, 54, 17}
	cosd := int32(12)
	//cosb := int32(12)
	//cosb := int32(6)

	fmt.Println(cd)
	fmt.Printf("Looking for cost : %v \n", cosd)

	whatFlavorsx(cd, cosd)
	//whatFlavorsConcurrent(cd, cosd)

}
