package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func sortArr(arr *[]int32) []int32 {
	newArr := *arr
	//{10, 20, 20, 10, 10, 30, 50, 10, 20}
	//fmt.Println(len(newArr))
	for x := int32(0); x < int32(len(newArr)); x++ {
		for j := int32(1); j < x; j++ {
			if newArr[j] > newArr[x] {
				inter := newArr[j]
				newArr[j] = newArr[x]
				newArr[x] = inter
			}
		}
	}
	fmt.Println(newArr)
	return newArr
}

//return number of similarly occuring given int are in given array
func similar(num int32, arr *[]int32) int32 {
	newArr := *arr
	counting := int32(0)

	for x := int32(0); x < int32(len(newArr)); x++ {
		if newArr[x] == num {
			counting++
		}
	}

	//pairs := counting / 2
	//fmt.Printf("Pairs in array : %v", pairs)
	return counting
}

//
func wasThere(val int32, arr *[]int32) (int, bool) {
	narr := *arr
	//x := position
	for i, items := range narr {
		if items == val {
			return i, true
		}
	}

	return -1, false
}

// Complete the sockMerchant function below.
func sockMerchant(n int32, arr []int32) int32 {
	//func sockMerchant() int32 {
	//n = 9
	//arr := []int32{10, 20, 20, 10, 10, 30, 50, 10, 20} // 3 pair
	//arr := []int32{1, 2, 1, 2, 1, 3, 2} // 2 pair
	//arr := []int32{10, 10, 10, 20, 20, 10, 10, 30, 1, 1, 1, 43, 43, 4, 50, 10, 20} // 6 pair
	countedArr := make([]int32, 0)
	//for x := 0; x < len(arr); x++ {
	for x := int32(0); x < n; x++ {
		//wast := wasThere(arr[x], &arr)
		//fmt.Println(wast)
		//if x > 0 {
		if similar(arr[x], &arr) < 2 {
			//fmt.Printf("\n This %v is got just one item \n", arr[x])

		} else {
			sim := similar(arr[x], &arr)
			pair := sim / 2
			//fmt.Printf("\n This %v appears %v times and has %v pairs", arr[x], sim, pair)
			_, isthere := wasThere(arr[x], &countedArr)
			if isthere == false {
				for y := int32(0); y < pair; y++ {
					countedArr = append(countedArr, arr[x])
				}
			}
		}
		//}

		//numx := sim / 2
		//part += numx
	}

	//fmt.Println(countedArr)

	return int32(len(countedArr))
}

func checkError(err error) {
	if err != nil {
		//panic(err)
		fmt.Println(err.Error())
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func main() {
	/*arr := []int32{10, 10, 10, 20, 20, 10, 10, 30, 1, 1, 1, 43, 43, 4, 50, 10, 20} // 6 pair
	n := int32(len(arr))
	pairs := sockMerchant(n, arr)
	fmt.Println(pairs)*/

	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	checkError(err)
	n := int32(nTemp)

	arTemp := strings.Split(readLine(reader), " ")

	var ar []int32

	for i := 0; i < int(n); i++ {
		arItemTemp, err := strconv.ParseInt(arTemp[i], 10, 64)
		checkError(err)
		arItem := int32(arItemTemp)
		ar = append(ar, arItem)
	}

	result := sockMerchant(n, ar)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()

}
