package main 

import(
	"fmt"
)

// Complete the rotLeft function below.
func rotLeft(a []int32, d int32) []int32 {
	newArx := make([]int32, 0)
	counter := 0;

	for x := d; x < int32(len(a)); x++ {
		newArx = append(newArx, a[x])
		counter++
	}
	for y := 0; y < len(a)-counter; y++ {
		newArx = append(newArx, a[y])
	}
	return newArx
}

func main() {
	//arr := make([]int, 0)
	arr := []int32{1, 2, 3, 4, 5}
	rot := int32(2)
	//fmt.Println(arr)
	fmt.Println(rotLeft(arr, rot))
}