package main

//LOGIC
/*
	SORT ARRAY
	SCAN FOR PAIRS FROM START TO FINISH
	COUNT PAIRS
	RESTART SCAN
	RETURN TOTAL PAIRS
*/
import (
	"fmt"
	"sort"
)

func scanArray(Q []int32, TotalPair int32) int32 {
	fmt.Println(Q)
	if len(Q) < 2 {
		return TotalPair
	}
	for x := 0; x < len(Q)-1; x++ {
		Ql := len(Q)
		switch Ql {
		case 0:
			return TotalPair
		case 1:
			return TotalPair
		default:
			if Q[x] == Q[x+1] {
				TotalPair++
				x++
			} else {
				//SKIP ANY INCREMENTS
			}
		}
	}
	return TotalPair
}

// Complete the sockMerchant function below.
func sockMerchant(N int32, Q []int32) int32 {
	sort.Slice(Q, func(i, j int) bool { return Q[int32(i)] < Q[int32(j)] })
	tp := scanArray(Q, 0)
	return tp
}

func main() {
	socks := []int32{10, 20, 20, 10, 10, 30, 50, 10, 20}
	//socks := []int32{10, 20, 20, 10, 10, 30, 50, 10, 20, 30, 40, 40, 21, 21, 92, 92, 100, 100}
	tp := sockMerchant(int32(len(socks)), socks)
	fmt.Println(tp)
}
