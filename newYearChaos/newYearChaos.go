package main

import (
	"fmt"
)

/*
//"sort"
func getBribes(q []int32, x int32) {
	moves := int32(0)
	//chaos := false

	//for x := int32(0); x < int32(len(q)-1); x++ {

	//NUMBER OF MEMBERS TO THE RIGHT THAT ARE LOWER THAN IT
	//1, 2, 5, 3, 7, 8, 6, 4
	counter := 0
	for y := int32(0); y < int32(len(q))-x; y++ {
		pad := y + x

		if q[x] > q[pad] {
			moves++
			counter++
		}

		if counter > 2 {
			//chaos = true
			fmt.Println("Too chaotic")
			return
		}
	}
	//}

	fmt.Println(moves)

	if x <= int32(len(q)) {
		x++
	} else {

	}
	getBribes(q, x)
}
*/
//minBribes returns number of minimum bribes
func minBribes(Q []int32, I int32) int32 {
	//fmt.Println(Q) //2, 1, 5, 3, 4
	//count := 0

	L := int32(len(Q))
	for x := L - 1; x >= 0; x-- {
		if Q[x] == L {
			diff := L - 1 - x

			switch diff {
			case 0:
				return minBribes(Q[:L-1], I)
			case 1:
				sw := Q[x]
				Q[x] = Q[x+1]
				Q[x+1] = sw
				I++
				return minBribes(Q[:L-1], I)
			case 2:
				sw := Q[x]
				Q[x] = Q[x+1]
				Q[x+1] = Q[x+2]
				Q[x+2] = sw
				I += 2
				return minBribes(Q[:L-1], I)
			case 3:
				fmt.Println("Too chaotic")
				return -1
			default:
				return -1
			}
		}
	}
	//if I > 0 {
	//	fmt.Println("Too chaotic")
	//} else {
	fmt.Println(I)
	//}
	return I
}

func main() {
	//cases := 2
	que := [][]int32{{2, 1, 5, 3, 4}, {2, 5, 1, 3, 4}, {1, 2, 5, 3, 4, 7, 8, 6}, {1, 2, 5, 3, 7, 8, 6, 4}}

	for x := 0; x < len(que); x++ {
		minBribes(que[x], 0)
	}

}

/*
//CALL
func Calc(Q []int32, A int32) int32 {
	//1, 2, 5, 3, 7, 8, 6, 4    L = 8
	//0, 1, 2, 3, 4, 5, 6, 7    i
	L := int32(len(Q))
	fmt.Printf("Slice of it : %v \n", Q)
	//fmt.Printf("Slice of it : %v \n", Q[:L-1])
	for i := L - 1; i >= 0; i-- {
		if Q[int32(i)] == L { //if 4 ==
			diff := L - 1 - i
			switch diff {
			case 0:
				return Calc(Q[:L-1], A)
			case 1:
				t := Q[i]
				Q[i] = Q[i+1]
				Q[i+1] = t
				A += 1
				return Calc(Q[:L-1], A)
			case 2:
				t := Q[i]
				Q[i] = Q[i+1]
				Q[i+1] = Q[i+2]
				Q[i+2] = t
				A += 2
				return Calc(Q[:L-1], A)
			default:
				return -1
			}
		}
	}
	return A
}

func getB(Q []int32) {
	//fmt.Println(N, M, Q)
	move := Calc(Q, 0)

	if move >= 0 {
		fmt.Println(move)
	} else {
		fmt.Println("Too chaotic")
	}
}
*/
