package main

import(
	"fmt"
	"sort"
	//"strings"
)


func hourGlassFit(arr [][]int32, HourGlassTopWidth, HourGlassLowWidth, HourGlassMidWidth, HourGlassHeight int ) int32 {
	HourGlassTopWidth = 3 
	HourGlassLowWidth = 3 
	HourGlassMidWidth = 1
	HourGlassHeight = 3   
	//fmt.Println(len(arr[0]))

	//HGCount := 0 
	//temArrWidth := len(arr)
	
	HGColCount := 0
	HGRowCount := 0
	HGSums := make([]int32, 0)

	for x := 0; x < len(arr); x++ {
		
		if HourGlassTopWidth <= (len(arr)-x) {
			HGColCount++
		}
		if HourGlassHeight <= (len(arr[0])-x) {
			HGRowCount++
		}
		
	}
	TotalHG := HGRowCount*HGColCount

	for g := 0; g < TotalHG; g++ {
		//fmt.Printf("Total Hour Glasses : %v \n", TotalHG)
		//fmt.Printf("Rows : %v \n", len(arr[0]))
		
		//for x := 0; x < len(arr); x++ {
		for x := 0; x < HGColCount; x++ {
		if g+2 < len(arr[0]) {	
			HG1 := arr[g][x] + arr[g][x+1] + arr[g][x+2]
			HG2 := arr[g+1][x+1]
			HG3 := arr[g+2][x] + arr[g+2][x+1] + arr[g+2][x+2] //check for veritcal array range overflow
			//HG3 := HG2
			newTotals := HG1 + HG2 + HG3
			HGSums = append(HGSums, newTotals)
			//fmt.Println(HGSums)
		}
	}
		//sort.Slice(HGSums, func(i, j int) bool { return HGSums[i] < HGSums[j] })
		//fmt.Println(HGSums)
	}
	/*fmt.Printf("Hour Glass Colums : %v \n", HGColCount)
	fmt.Printf("Hour Glass Rows : %v \n", HGRowCount)
	fmt.Printf("Total Possible HowGlasses : %v \n", HGColCount*HGRowCount)
	*/
	//fmt.Println(HGSums)
	sort.Slice(HGSums, func(i, j int) bool { return HGSums[i] < HGSums[j] })
	//fmt.Println(HGSums)
	
	return HGSums[len(HGSums)-1]
}

func hourGlassSum(arr [][]int32) int32 {
	//fmt.Println(arr[1][0])
	HourGlassTopWidth := 3
	HourGlassLowWidth := 3
	HourGlassMidWidth := 1
	HourGlassHeight := 3
	//Dynamically calculate howmany Glasses would fit in array
	HourGlassTotal := hourGlassFit(arr, HourGlassTopWidth, HourGlassLowWidth, HourGlassMidWidth, HourGlassHeight)
	//fmt.Println(HourGlassTotal)
	//:::::::::::::::::::::
	return HourGlassTotal //0
}

func main() {
	arr := [][]int32{{1, 1, 1, 0, 0, 0},{0, 1, 0, 0, 0, 0},{1, 1, 1, 0, 0, 0},{0, 0, 2, 4, 4, 0},{0, 0, 0, 2, 0, 0},{0, 0, 1, 2, 4, 0},}
	//fmt.Println(arr)
	fmt.Println(hourGlassSum(arr))
}

/*
package main

/*
import (
	"fmt"
	"math"
)

func main() {
	arr := make([][]int, 6)
	for i := 0; i < 6; i++ {
		arr[i] = make([]int, 6)
		for j := 0; j < 6; j++ {
			fmt.Scan(&arr[i][j])
		}
	}
	max := math.MinInt32
	j := 0
	i := 0
	for j < 4 {
		tmp := arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]
		if tmp > max {
			max = tmp
		}
		i++
		if i == 4 {
			i = 0
			j++
		}
	}
	fmt.Println(max)
}
*/