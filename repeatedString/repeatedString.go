package main

import (
	"fmt"
)

func repeatedString(s string, n int64) int64 {

	//makeStr := make([]string, 0)
	makeStx := make([]byte, 0)

	sbyte := []byte(s)

	sSize := len(sbyte) //size of repeating string

	for x := 0; x < sSize; x++ {
		stringsLeft := n - int64(len(makeStx)) //amoun of string to be added
		//fmt.Println(stringsLeft)
		/*
			if stringsLeft > 3 {
				makeStx = append(makeStx, sbyte[x])
				makeStx = append(makeStx, sbyte[x+1])
				//makeStx = append(makeStx, sbyte[x+2])
				if x == sSize-1 {
					x = x - sSize //reset x to start from the first character in repeating string
				}
			}

			if stringsLeft > 2 {
				makeStx = append(makeStx, sbyte[x])
				makeStx = append(makeStx, sbyte[x+1])
				if x == sSize-1 {
					x = x - sSize //reset x to start from the first character in repeating string
				}
			}*/

		if stringsLeft > 0 {
			makeStx = append(makeStx, sbyte[x])
			if x == sSize-1 {
				x = x - sSize //reset x to start from the first character in repeating string
			}
		}

	}
	//fmt.Println(makeStx)
	aN := []byte("a")

	countA := int64(0)
	for x := 0; x < len(makeStx); x++ {
		if makeStx[x] == aN[0] {
			countA++
		}
	}
	fmt.Println("count of A :")
	fmt.Println(countA)

	return countA
}

func main() {
	s := "aba"
	n := int64(1000000000000)

	countStr := repeatedString(s, n)
	fmt.Println(countStr)
}
