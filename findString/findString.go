package main

import (
	"fmt"
	//"strings"
	"bytes"
)

func main(){
	//find the number of time subStr occours in mainString
	//main string
	mainString := "ABCBABCYCSABCABCBCAXCA"
	num := []byte(mainString)

	//Substring
	subStr := "ABC"
	nstr := []byte(subStr)
	nstrN := len(nstr)


	//hold string the size of subStr to check for occurence through comparison
	tempSlice := make([]byte, nstrN)

	//Store the number of times the target string occurs
	occourrenceCount := 0;

	for x := 0; x < len(num); x++ {

			for y := 0; y < nstrN; y++ {
				if x+y < len(num){ //check for range of index
					tempSlice[y] = num[x+y]
				}else{
					//Gone out of range
			}
		}
		//compare
		if(bytes.Compare(tempSlice, nstr) == 0){ //compare
			occourrenceCount++
		}
	}
	fmt.Println(occourrenceCount)
}