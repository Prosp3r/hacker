package main

import "fmt"

func firstDupInt(a []int) int {
	seen := make(map[int]struct{})
	for _, v := range a {
		if _, exists := seen[v]; exists {
			return v
		}
		seen[v] = struct{}{}
	}
	return -1
}

func main() {
	c := []int{2, 3, 3, 1, 5, 2} //should return 3
	d := []int{1, 1, 2, 2, 1}    //should return 1
	fmt.Println(firstDupInt(c))
	fmt.Println(firstDupInt(d))
}
