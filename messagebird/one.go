package main

import "fmt"

func numberOfConnections(gridOfNodes [][]int32) int32 {
	// Write your code here
	G := gridOfNodes
	RowCount := len(G)
	nodeCount := make([]int32, len(G[0]))
	for x := 0; x < RowCount; x++ {
		colCount := len(G[x])
		for o := 0; o < colCount; o++ {
			if G[x][o] == 1 {
				nodeCount[x]++
			}
		}
		//fmt.Println(nodeCount[x])
	}

	connections := int32(0)
	skip := int32(0)
	for c := int32(1); c < int32(len(nodeCount)); c++ {
		if nodeCount[c] < 1 {
			//skip
			skip = 1
		} else {

			cons := nodeCount[c] * nodeCount[c-(1+skip)]
			connections += cons
			skip = 0
		}
	}
	//fmt.Println(connections)

	return connections
}

func main() {
	grid := [][]int32{{1, 0, 1, 1}, {0, 1, 1, 0}, {0, 0, 0, 0}, {1, 0, 0, 0}}
	//numberOfConnections(grid)
	fmt.Printf("Count returned %v connections", numberOfConnections(grid))
}
