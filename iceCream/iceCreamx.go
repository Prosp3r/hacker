package main

import (
	"fmt"
	"sort"
)

//Flav holds flavours
type Flav struct {
	id   int
	cost int32
}

type flavSort []*Flav

//var flav = make(map[int]int32)

func (flav flavSort) Len() int {
	return len(flav)
}

func (flav flavSort) Less(f, s int) bool {
	return flav[f].cost < flav[s].cost
}

func (flav flavSort) Swap(f, s int) {
	flav[f], flav[s] = flav[s], flav[f]
}

func whatFlavors(cost []int32, money int32) int {
	/*//flav := make([]*Flav, len(cost))
	for i, c := range cost {
		flav[i] = &Flav{
			id:   i + 1,
			cost: c,
		}
	}*/
	flav := make(map[int]int32)
	for i, c := range cost {
		flav = Flav{
			id:   i + 1,
			cost: c,
		}
	}

	fmt.Println(flav)
	sort.Sort(flavSort(flav))
	fmt.Println(flav)
	return 0
}

func binsearch() {}

func main() {
	cb := []int32{7, 2, 5, 4, 11}
	cosb := int32(12)

	fmt.Println(cb)
	fmt.Printf("Finding sum for : %v \n", cosb)
	whatFlavors(cb, cosb)

	//ca := []int32{1, 4, 5, 3, 2}
	ca := []int32{4, 3, 2, 5, 7}
	cosa := int32(8)
	fmt.Println(ca)
	fmt.Printf("Finding sum for : %v \n", cosa)
	whatFlavors(ca, cosa)
}
