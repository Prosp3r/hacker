package main

import (
	"fmt"
	"time"
	"sort"
)

func getMaxProfit(price1, price2 int32) int32 {
	return price1 - price2
	//return 0
}

func bsort(arr []int32) []int32 {

	for x := 0; x < len(arr); x++ {
		for y := x + 1; y < len(arr); y++ {
			if arr[x] > arr[y] {
				inter := arr[x]
				arr[x] = arr[y]
				arr[y] = inter
			}
		}
	}
	//fmt.Println(arr)
	return arr
}

func measureTime(funcName string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("Time taken by %s function is %v \n", funcName, time.Since(start))
	}
}

func measureProfit(sPrice []int) int {
	defer measureTime("measureProfit")()
	profits := make([]int, 0)
	//sz := len(sPrice)
	//profitx := make([][]int32, 0)

	///profit := make(map[int]int32)
    for i := range sPrice {
		for x := i+1; x < len(sPrice); x++ {
			profits = append(profits, sPrice[x] - sPrice[i])
		}
	}
	fmt.Println(profits)
	//fmt.Println(profit)
	sort.Ints(profits)
	//sprofit := bsort(profits)
	maxP := profits[len(profits)-1]
	//fmt.Println(profits)
	fmt.Println(maxP)
	return maxP
	//return 0
}

func checkProfit(sPrice map[int]int32) int32 {
	defer measureTime("checkProfit")()
	profits := make(map[int]int32)
	//maps
	mapKey := 0
	for i := range sPrice {
		for x := i+1; x < len(sPrice); x++ {
			//profits = append(profits, sPrice[i] - sPrice[x])
			profits[mapKey] = sPrice[x] - sPrice[i]
			mapKey++
		}
	}
	fmt.Println("profits :")
	fmt.Println(profits)
	return 0
}

func main() {
	sPrice := []int{10, 7, 5, 8, 11, 9}
	//sPrice := []int{10, 7, 5, 8, 11, 9, 100, 2, 40, 210, 500, 32, 21, 98}

	mPrice := make(map[int]int)
	for i, p := range sPrice {
		//fmt.Printf("index: %v => Value: %v \n", i, p)
		mPrice[i] = p
	}
	//fmt.Println(bsort(sPrice))
	fmt.Println(mPrice)
	measureProfit(sPrice)
	//checkProfit(mPrice)
	//measProf(sPrice)
	fmt.Println("Finished executing main")

}

/*func main() {
	defer measureTime("main")()
	var bestTrade struct {
		buy, sell int
	}

	stock_prices := []int{10, 7, 5, 8, 11, 9}
	//stock_prices := []int{10, 7, 5, 8, 11, 9, 100, 2, 40, 210, 500, 32, 21, 98}
	fmt.Println(stock_prices)
	for i, buy := range stock_prices {
		for _, sell := range stock_prices[i:] {
			if (buy - sell) > (bestTrade.buy - bestTrade.sell) {
				bestTrade.buy = buy
				bestTrade.sell = sell
			}
		}
	}

	fmt.Printf("buy: %d, sell %d\n", bestTrade.buy, bestTrade.sell)
}
*/
