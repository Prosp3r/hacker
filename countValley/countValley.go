package main

import "fmt"

// Complete the countingValleys function below.
func countingValleys(n int32, s string) int32 {
	sl := 0
	val := 0
	mnt := 0
	belowSea := int32(0)
	a := []rune(s)

	for _, r := range a {
		if r == 'U' {
			mnt++
			sl++
		}
		if r == 'D' {
			val++
			sl--
			//this only happens once
			if sl == -1 {
				belowSea++
			}
		}
	}

	return belowSea
}

func main() {
	s := "UDDDUDUU"
	climbs := countingValleys(8, s)
	fmt.Println(climbs)
}
