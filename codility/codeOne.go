package main

/*
import (
	"fmt"
)

func main() {
	fv := "Hey"
	fmt.Println(fv)
}*/

// you can also use imports, for example:
// import "fmt"
// import "os"
import (
	"fmt"
	"sort"
)

// you can write to stdout for debugging purposes, e.g.
// fmt.Println("this is a debug message")
//var absent = make([]int, 0)

//Solution from codility
func Solution(A []int) int {
	// write your code in Go 1.4
	sort.Ints(A)
	fmt.Println(A)
	//absent := make([]int, 0)
	y := 0
	for _, v := range A {
		if v > 0 {
			y++
			if y == v {
				//its there
			} else {
				return y
			}
		}
	}
	return 1
}

func main() {
	//arr := []int{-1, 0, 1, 2, 3, 5, 6, 7}
	arr := []int{-1, -3}
	fmt.Println(Solution(arr))
}
