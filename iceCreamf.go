package main

import (
	"fmt"
	"time"
)

/*func whatFlavors(cost []int32, money int32) {

	sz := int32(len(cost))
	for x := int32(0); x < sz; x++ {

		for y := int32(sz - 1); y >= 0; y-- {
			if x == y {
				//skip
			} else {
				if cost[x]+cost[y] == money {

					val0 := y + 1
					val1 := x + 1
					fmt.Println(val1, val0)
					//fmt.Println(val1)
					return
				}
			}
		}

	}
}*/

func measureTimec(funcName string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("Time taken by %s function is %v \n\n\n", funcName, time.Since(start))
	}
}

func whatFlavorsc(cost []int32, money int32) {
	defer measureTimec("whatFlavors")()
	sz := int32(len(cost))
	//match := make(map[int32]struct{})
	for i, v := range cost {
		//1, 4, 5, 3, 2
		for x := int32(i + 1); x < sz; x++ {
			if v+cost[x] == money {
				fmt.Println(i+1, x+1)
			}
		}
	}
}

func factCitec(cost []int32, money, i, y int32) int {

	if y >= int32(len(cost)) {
		return 0
	}

	if cost[i]+cost[y] == money {
		fmt.Printf("%v %v \n", i+1, y+1)
		return 0
	}
	y++
	return factCitec(cost, money, i, y)
}

//
/*func factCost(cost []int32, money int32, i int32) int {
	//fmt.Println(cost)

	//factCite(cost, money, i, i+1)
	for y := i + 1; y < int32(len(cost)); y++ {
		if cost[i]+cost[y] == money {
			fmt.Printf("%v %v \n", i+1, y+1)
			return 0
		}
	}

	if i >= int32(len(cost)-1) {
		return 0
	}
	i++
	return len(cost) * factCost(cost, money, i)
}*/

//XCosts holding cost structure
type XCosts struct {
	id   int32
	cost int32
}

/*Define interface for using sort*/
type byCost []*XCosts

//Len part of the sort.Sort interface
func (c byCost) Len() int32 {
	return int32(len(c))
}

func (c byCost) Swap(i, j int32) {
	c[i], c[j] = c[j], c[i]
}

func (c byCost) Less(i, j int32) bool {
	return c[i].id < c[j].id
}

/*END interface type definitions*/

func binSearch(mcost []XCosts, lowpoint, highpoint, hmoney int32) int32 {
	midpoint := (lowpoint + (highpoint-lowpoint)/2)
	//if midpoint+1 > highpoint {
	//	return 0
	//}
	for lowpoint <= highpoint {
		//INTERESTING EXPERIMENT to EXTEND BINARY SEARCH FOR SPEED
		if mcost[midpoint+1].cost == hmoney {
			return midpoint + 1
		}
		if mcost[midpoint-1].cost == hmoney {
			return midpoint - 1
		}
		//END OF EXPERIMENT

		midpoint = (lowpoint + (highpoint-lowpoint)/2)
		if mcost[midpoint].cost < hmoney {
			lowpoint = midpoint + 1
			midpoint = (lowpoint + (highpoint-lowpoint)/2)
		} else if mcost[midpoint].cost > hmoney {
			highpoint = midpoint - 1
			midpoint = (lowpoint + (highpoint-lowpoint)/2)
		} else {
			return midpoint
		}
	}
	//if all fails
	return 0
}

func factCostc(cost []int32, money int32) /*(int32, int32)*/ {
	mcost := make([]XCosts, len(cost)) //sliced cost
	k := 0
	for i, v := range cost {
		mcost[k] = XCosts{
			int32(i) + 1, v,
		}
		k++
	}

	for i, n := int32(0), int32(len(mcost)); i < n; i++ {
		hmoney := money - mcost[i].cost
		//fmt.Println(i, n)
		result := binSearch(mcost, i+1, n-1, hmoney)
		if result > 0 {
			fmt.Println(i, result)
			fmt.Println(swapLoHi(i+1, result+1))
			return
			//return swapLoHi(mcost[i], result)
			//return swapLoHi(i, result)
		}
	}
	//return 0, 0
}

func swapLoHi(i, j int32) (int32, int32) {
	if i > j {
		return j, i
	}
	return i, j
}

//WITH MAPS
func whatFlavorsxc(cost []int32, money int32) {
	defer measureTimec("whatFlavorsxxx")()
	factCostc(cost, money)
}

func main() {
	//
	cb := []int32{2, 2, 4, 3}
	//2 2 4 3
	//cb := []int32{1, 4, 5, 3, 2}
	cosb := int32(4)
	//cb := []int32{7, 2, 5, 4, 11}
	//cosb := int32(12)
	//cosb := int32(6)

	fmt.Println(cb)
	fmt.Printf("Looking for cost : %v \n", cosb)

	whatFlavorsxc(cb, cosb)

	//ca := []int32{1, 4, 5, 3, 2}
	ca := []int32{4, 3, 2, 5, 7}
	cosa := int32(8)
	fmt.Println(ca)
	fmt.Printf("Looking for cost : %v \n", cosa)

	whatFlavorsxc(ca, cosa)

	cc := []int32{1, 2, 3, 5, 6}
	cosc := int32(5)
	fmt.Println(cc)
	fmt.Printf("Looking for cost : %v \n", cosc)

	whatFlavorsxc(cc, cosc)
	//factCost(ca, cosa, 0)

	//cd := []int32{2, 2, 4, 3}
	//2 2 4 3
	//cd := []int32{1, 4, 5, 3, 2}
	cd := []int32{7, 2, 5, 4, 11}
	cosd := int32(12)
	//cosb := int32(12)
	//cosb := int32(6)

	fmt.Println(cd)
	fmt.Printf("Looking for cost : %v \n", cosd)

	whatFlavorsxc(cd, cosd)
	//4

}
