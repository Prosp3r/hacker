package china

import (
	"sort"
)

/*func main() {
	var (
		t    int
		m    int
		n    int
		a    []int
		line string
		err  error
	)
	r := bufio.NewReader(os.Stdin)

	line, _ = r.ReadString('\n')

	t, _ = strconv.Atoi(strings.Trim(line, " \n\t"))

	for i := 0; i < t; i++ {
		line, _ = r.ReadString('\n')

		m, _ = strconv.Atoi(strings.Trim(line, " \n\t"))

		line, _ = r.ReadString('\n')

		n, _ = strconv.Atoi(strings.Trim(line, " \n\t"))

		a = make([]int, n)

		line, err = r.ReadString('\n')

		line = strings.Trim(line, " \n\t")

		numbers := strings.Fields(line)

		i := 0
		for _, number := range numbers {
			a[i], err = strconv.Atoi(number)
			if err == nil {
				i++
			}
			if i >= n {
				break
			}
		}
		fmt.Println(a)
		fmt.Println(choose(a, m))
	}
}*/

type icecream struct {
	id   int
	cost int
}

//IceCream typs
type costSort []*icecream

func (c costSort) Len() int {
	return len(c)
}

//Returns True of false
func (c costSort) Less(i int, j int) bool {
	return c[i].cost < c[j].cost
}

//Swaps
func (c costSort) Swap(i int, j int) {
	c[i], c[j] = c[j], c[i]
}

func choose(a []int, m int) (min, max int) {
	parlor := make([]*icecream, len(a))
	for i, cost := range a {
		parlor[i] = &icecream{
			id:   i + 1,
			cost: cost,
		}
	}
	sort.Sort(costSort(parlor))

	for i, n := 0, len(parlor); i < n-1; i++ {
		other := m - parlor[i].cost
		j := find(parlor, i+1, n-1, other)
		if j > 0 {
			return s(parlor[i].id, j)
		}
	}
	return
}

//binary search
func find(a []*icecream, low, high, m int) int {
	mid := (low + high) / 2
	for low <= high {
		if a[mid].cost < m {
			low = mid + 1
			mid = (low + high) / 2
		} else if a[mid].cost > m {
			high = mid - 1
			mid = (low + high) / 2
		} else {
			return a[mid].id
		}
	}
	return -1
}

//returns the numbers in accending order for display purpose only
func s(a, b int) (int, int) {
	if a < b {
		return a, b
	}
	return b, a
}
